#!/bin/sh
g++ \
src/*.cpp src/Game/*.cpp src/Input/*.cpp src/Midi/*.cpp src/Render/*.cpp src/Song/*.cpp src/Utilities/*.cpp \
-o game \
-std=c++17 \
-I/usr/include/rtmidi \
-Isrc \
-lpthread \
-lrtmidi \
-lsfml-graphics \
-lsfml-window \
-lsfml-audio \
-lsfml-system