#pragma once


#include <chrono>
#include <string>


class MidiEvent {

public:
    enum Type {
        MIDI_NOTE_OFF = 8,
        MIDI_NOTE_ON,
        MIDI_AFTERTOUCH,
        MIDI_CONTROL_CHANGE,
        MIDI_PROGRAM_CHANGE,
        MIDI_AFTERTOUCH2,
        MIDI_PITCH_BEND
    };

    std::chrono::system_clock::time_point timestamp;

    Type getType();
    unsigned char getChannel();
    unsigned char getKey();
    unsigned char getVel();
    unsigned short getPitchBend();

    void setType(Type);
    void setChannel(unsigned char);
    void setKey(unsigned char);
    void setVel(unsigned char);
    void setPitchBend(unsigned short);

    std::string str();

    MidiEvent();
    MidiEvent(unsigned char, unsigned char, unsigned char);

private:
    unsigned char byte1;
    unsigned char byte2;
    unsigned char byte3;

};
