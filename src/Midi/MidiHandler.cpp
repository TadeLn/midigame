#include <RtMidi.h>

#include "Midi/MidiHandler.hpp"
#include "Midi/MidiEvent.hpp"
#include "Utilities/Error.hpp"

#ifndef QUEUE_LIMIT
#define QUEUE_LIMIT 1000
#endif


void MidiHandler::initializeIn() {
    try {
        midiin = new RtMidiIn;
    } catch (RtMidiError &error) {
        Error::throwErr(HERE, std::string("RtMidi could not be initialized: ") + error.getMessage());
    }
    midiInInitialized = true;
}

void MidiHandler::initializeOut() {}

bool MidiHandler::isInInitialized() {
    return midiInInitialized;
}

bool MidiHandler::isOutInitialized() {
    return midiOutInitialized;
}

void MidiHandler::checkInInitialized() {
    if (!isInInitialized()) {
        Error::throwErr(HERE, "RtMidiIn was not initialized!");
    }
}

void MidiHandler::checkOutInitialized() {
    if (!isOutInitialized()) {
        Error::throwErr(HERE, "RtMidiOut was not initialized!");
    }
}

bool MidiHandler::pollEvent(MidiEvent* ptr, std::string portName) {
    unsigned int size = events.size();
    if (size > 0) {
        *ptr = events[0];
        events.erase(events.begin());
        return true;
    }
    return false;
}



unsigned int MidiHandler::getPortCount() {
    checkInInitialized();
    return midiin->getPortCount();
}

std::string MidiHandler::getPortName(unsigned int portNumber) {
    checkInInitialized();
    return midiin->getPortName(portNumber);
}



void MidiHandler::openPort(unsigned int portNumber = 0) {
    checkInInitialized();
    if (midiin->isPortOpen()) {
        Error::throwErr(HERE, "Port is already opened!");
    }
    midiin->openPort(portNumber);
    midiin->ignoreTypes(true, true, true);
    midiin->setCallback(&MidiHandler::_onMidiEvent, this);
}

bool MidiHandler::isPortOpen(unsigned int portNumber = 0) {
    checkInInitialized();
    return midiin->isPortOpen();
}

void MidiHandler::closePort() {
    checkInInitialized();
    if (!isPortOpen()) {
        Error::throwErr(HERE, "No port is open!");
    }
    return midiin->closePort();
}



void MidiHandler::_onMidiEvent(double deltaTime, std::vector<unsigned char>* message, void* ptr) {
    if (message->size() == 0) return;

    MidiHandler* h = (MidiHandler*)ptr;
    MidiEvent event;
    event.setType((MidiEvent::Type)(message->at(0) >> 4));
    event.setChannel((MidiEvent::Type)(message->at(0) % 16));

    if (message->size() > 1) {
        switch (event.getType()) {

            case MidiEvent::MIDI_NOTE_ON:
                event.setKey(message->at(1));
                event.setVel(127);
                break;

            case MidiEvent::MIDI_NOTE_OFF:
                event.setKey(message->at(1));
                event.setVel(0);
                break;

            case MidiEvent::MIDI_PITCH_BEND:
                if (message->size() > 2) {    
                    event.setPitchBend((message->at(2) * 128) + message->at(1));
                }
                break;
        }
    }

    h->_pushEvent(event);
}

void MidiHandler::_pushEvent(MidiEvent e) {
    events.push_back(e);
    while (events.size() > QUEUE_LIMIT) {
        events.erase(events.begin());
    }
}

MidiHandler::MidiHandler() {
    RtMidiIn* midiin = nullptr;
    midiInInitialized = false;
}
