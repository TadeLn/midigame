#pragma once


#include <string>
#include <vector>
#include <RtMidi.h>

#include "Midi/MidiEvent.hpp"


class MidiHandler {

public:
    void initializeIn();
    void initializeOut();

    bool isInInitialized();
    bool isOutInitialized();

    void checkInInitialized();
    void checkOutInitialized();

    bool pollEvent(MidiEvent*, std::string);

    unsigned int getPortCount();
    std::string getPortName(unsigned int);

    void openPort(unsigned int);
    bool isPortOpen(unsigned int);
    void closePort();

    static void _onMidiEvent(double, std::vector<unsigned char>*, void*);
    void _pushEvent(MidiEvent);

    MidiHandler();

private:
    RtMidiIn* midiin;
    bool midiInInitialized;

    bool midiOutInitialized;

    std::vector<MidiEvent> events;

};