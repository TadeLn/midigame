#include <string>

#include "Utilities/Error.hpp"
#include "Midi/MidiEvent.hpp"


MidiEvent::Type MidiEvent::getType() {
    return (Type)(byte1 >> 4);
}

unsigned char MidiEvent::getChannel() {
    return byte1 % 16;
}

unsigned char MidiEvent::getKey() {
    if (getType() != MIDI_NOTE_OFF && getType() != MIDI_NOTE_ON) Error::throwErr(HERE, "This MIDI event is not a note event!");
    return byte2 % 128;
}

unsigned char MidiEvent::getVel() {
    if (getType() != MIDI_NOTE_OFF && getType() != MIDI_NOTE_ON) Error::throwErr(HERE, "This MIDI event is not a note event!");
    return byte3 % 128;
}

unsigned short MidiEvent::getPitchBend() {
    if (getType() != MIDI_PITCH_BEND) Error::throwErr(HERE, "This MIDI event is not a pitch bend event!");
    return ((byte3 % 128) * 128) + (byte2 % 128);
}

void MidiEvent::setType(Type type) {
    byte1 &= 0x0f;
    byte1 |= (type << 4);
}

void MidiEvent::setChannel(unsigned char channel) {
    byte1 &= 0xf0;
    byte1 |= (channel % 16);
}

void MidiEvent::setKey(unsigned char key) {
    if (getType() != MIDI_NOTE_OFF && getType() != MIDI_NOTE_ON) Error::throwErr(HERE, "This MIDI event is not a note event!");
    byte2 = key % 128;
}

void MidiEvent::setVel(unsigned char vel) {
    if (getType() != MIDI_NOTE_OFF && getType() != MIDI_NOTE_ON) Error::throwErr(HERE, "This MIDI event is not a note event!");
    if (vel == 0) setType(MIDI_NOTE_OFF);
    byte3 = vel % 128;
}

void MidiEvent::setPitchBend(unsigned short pitchBend) {
    if (getType() != MIDI_PITCH_BEND) Error::throwErr(HERE, "This MIDI event is not a pitch bend event!");
    byte2 = pitchBend % 128;
    byte3 = (pitchBend >> 7) % 128;
}

std::string MidiEvent::str() {
    Type type = getType();
    std::string result(
        "t: " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(timestamp.time_since_epoch()).count()) +
        " #: " + std::to_string((unsigned int)getChannel()) +
        " T: " + std::to_string((unsigned int)getType())
    );
    if (type == MIDI_NOTE_OFF || type == MIDI_NOTE_ON) {
        result += std::string(
            " K: " + std::to_string((unsigned int)getKey()) +
            " V: " + std::to_string((unsigned int)getVel())
        );
    }
    if (type == MIDI_PITCH_BEND) {
        result += std::string(
            " PB: " + std::to_string(getPitchBend())
        );
    }
    return result;
}

MidiEvent::MidiEvent() {
    timestamp = std::chrono::system_clock::now();
    this->byte1 = 0;
    this->byte2 = 0;
    this->byte3 = 0;
}

MidiEvent::MidiEvent(unsigned char byte1 = 0, unsigned char byte2 = 0, unsigned char byte3 = 0) {
    timestamp = std::chrono::system_clock::now();
    this->byte1 = byte1;
    this->byte2 = byte2;
    this->byte3 = byte3;
}


