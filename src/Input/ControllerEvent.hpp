#pragma once


#include <chrono>
#include <string>


class ControllerEvent {

public:
    enum Type {
        EVENT_UNKNOWN,
        EVENT_PRESS,
        EVENT_RELEASE
    };

    Type type;
    unsigned char key;
    unsigned char vel;
    std::chrono::system_clock::time_point timestamp;
    
    std::string str();

    ControllerEvent();
    ControllerEvent(Type);
    ControllerEvent(Type, unsigned char, unsigned char, std::chrono::system_clock::time_point);

};