#include <string>
#include <thread>

#include "Input/Controller.hpp"
#include "Midi/MidiHandler.hpp"
#include "Utilities/Error.hpp"
#include "Input/MidiController.hpp"
#include "Utilities/Log.hpp"


bool MidiController::isPressed(unsigned char key) {
    if (key >= 128) {
        Error::throwErr(HERE, "There is no key " + std::to_string(key) + "!");
    }
    return keys[key];
}

unsigned char MidiController::getMidiChannel() { return midiChannel; }

void MidiController::setMidiChannel(unsigned char channel) {
    if (channel > 15) Error::throwErr(HERE, "Channel (" + std::to_string(channel) + ") cannot be above 15!");
    midiChannel = channel;
}

std::string MidiController::getMidiPort() { return midiPortName; }

void MidiController::setMidiPort(unsigned int portNumber) {
    if (portNumber >= midi->getPortCount()) Error::throwErr(HERE, "There is no port " + std::to_string(portNumber) + "!");
    midiPortName = midi->getPortName(portNumber);
}

void MidiController::listen(MidiController *c) {
    MidiEvent event;
    while (c->connected) {
        std::chrono::time_point time = std::chrono::system_clock::now();

        while (c->midi->pollEvent(&event, c->getMidiPort())) {
            if (event.getChannel() == c->getMidiChannel()) {
                ControllerEvent result(ControllerEvent::EVENT_UNKNOWN);
                switch (event.getType()) {

                    case MidiEvent::MIDI_NOTE_ON:
                        result.key = event.getKey();
                        result.vel = event.getVel();
                        result.type = ControllerEvent::EVENT_PRESS;
                        result.timestamp = event.timestamp;
                        break;

                    case MidiEvent::MIDI_NOTE_OFF:
                        result.key = event.getKey();
                        result.vel = event.getVel();
                        result.type = ControllerEvent::EVENT_RELEASE;
                        result.timestamp = event.timestamp;
                        break;

                }
                if (result.type != ControllerEvent::EVENT_UNKNOWN) {
                    c->_pushEvent(result);
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(CONTROLLER_MIDI_SLEEP_TIME) - (std::chrono::system_clock::now() - time));
    }
} 

MidiController::MidiController(MidiHandler* midiHandlerPtr, unsigned int port = 0, unsigned char channel = 0) {
    type = CONTROLLER_MIDI;

    midi = midiHandlerPtr;

    keys = new bool[128];
    for (unsigned int i = 0; i < 128; i++) {
        keys[i] = false;
    }

    setMidiPort(port);
    setMidiChannel(channel % 16);

    if (!midi->isPortOpen(port)) {
        midi->openPort(port);
    }

    connected = true;
    t_listen = std::thread(listen, this);
}

MidiController::~MidiController() {
    connected = false;
    t_listen.join();

    delete[] keys;
}

void MidiController::_pushEvent(ControllerEvent e) {
    if (!isActive()) return;
    events.push_back(e);
    while (events.size() > QUEUE_LIMIT) {
        events.erase(events.begin());
    }
    #ifdef LOG_CONTROLLER_EVENTS
        Log::log("Midi Controller Event: ", e.str());
    #endif
}
