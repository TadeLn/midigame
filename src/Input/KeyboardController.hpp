#pragma once


#include <thread>
#include <SFML/Graphics.hpp>

#include "Input/Controller.hpp"
#include "Input/ControllerEvent.hpp"
#include "Utilities/Error.hpp"
#include "Game/Game.hpp"


class KeyboardController : public Controller {

public:
    bool isPressed(unsigned char);

    static void listen(KeyboardController*);

    KeyboardController(Game*);
    ~KeyboardController();
    
    Game* getGame();

    void _pushEvent(ControllerEvent);

private:
    bool connected;
    bool* keys;
    std::thread t_listen;
    Game* g;

    sf::Keyboard::Key** scaleData;

};