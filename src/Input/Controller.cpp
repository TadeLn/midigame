#include "Input/Controller.hpp"
#include "Input/ControllerEvent.hpp"


bool Controller::isActive() {
    return active;
}

void Controller::activate() {
    active = true;
}

void Controller::deactivate() {
    active = false;
    dropEvents();
}

Controller::Type Controller::getType() {
    return type;
}

bool Controller::isPressed(unsigned char) {
    return false;
}

bool Controller::pollEvent(ControllerEvent* ptr) {
    if (events.size() > 0) {
        *ptr = events[0];
        events.erase(events.begin());
        return true;
    }
    return false;
}

void Controller::dropEvents() {
    events.clear();
}

Controller::Controller() {
    scale = Scale::C;
    transpose = 0;
    active = true;
}

Controller::~Controller() {
    deactivate();
}