#include <chrono>
#include <thread>
#include <string>
#include <SFML/Window.hpp>

#include "Input/Controller.hpp"
#include "Input/ControllerEvent.hpp"
#include "Utilities/Log.hpp"
#include "Utilities/Error.hpp"
#include "Game/Game.hpp"

#include "Input/KeyboardController.hpp"


#define CONTROLLER_KEYBOARD_KEY_COUNT 17

#ifndef CONTROLLER_KEYBOARD_SLEEP_TIME
#define CONTROLLER_KEYBOARD_SLEEP_TIME 10
#endif

#ifndef QUEUE_LIMIT
#define QUEUE_LIMIT 1000
#endif


bool KeyboardController::isPressed(unsigned char key) {
    if (key >= CONTROLLER_KEYBOARD_KEY_COUNT) {
        Error::throwErr(HERE, "Key index out of bounds (" + std::to_string(key) + ", expected at most " + std::to_string(CONTROLLER_KEYBOARD_KEY_COUNT) + ")");
    }
    return keys[key];
}

void KeyboardController::listen(KeyboardController* c) {

    while (c->connected) {
        std::chrono::time_point time = std::chrono::system_clock::now();
        std::vector<bool> keyboard = std::vector<bool>(c->g->getKeyboard());

        for (int i = 0; i < CONTROLLER_KEYBOARD_KEY_COUNT; i++) {
            if (c->scaleData[c->scale][i] == sf::Keyboard::Unknown) continue;
            if (keyboard[c->scaleData[c->scale][i]] != c->keys[i]) {
                c->keys[i] = !c->keys[i];
                if (c->keys[i]) {
                    c->_pushEvent(ControllerEvent(ControllerEvent::EVENT_PRESS, i + c->scale, 127, time));
                } else {
                    c->_pushEvent(ControllerEvent(ControllerEvent::EVENT_RELEASE, i + c->scale, 0, time));
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(CONTROLLER_KEYBOARD_SLEEP_TIME) - (std::chrono::system_clock::now() - time));
    }
}

KeyboardController::KeyboardController(Game* game) {
    g = game;
    type = CONTROLLER_KEYBOARD;
    keys = new bool[CONTROLLER_KEYBOARD_KEY_COUNT];
    for (unsigned int i = 0; i < CONTROLLER_KEYBOARD_KEY_COUNT; i++) {
        keys[i] = false;
    }

    connected = true;
    t_listen = std::thread(listen, this);

    {
        using namespace sf;
        typedef sf::Keyboard K; // sf::Keyboard
        const K::Key U = K::Unknown; // Unknown key
        const int C = CONTROLLER_KEYBOARD_KEY_COUNT; // Keyboard controller key count

        scaleData = new Keyboard::Key*[12];

        scaleData[Scale::C]      = new K::Key[C]{ K::A, K::W, K::S, K::E, K::D, K::F, K::T, K::G, K::Y, K::H, K::U, K::J, K::K, K::O, K::L, K::P, U    };
        scaleData[Scale::Csharp] = new K::Key[C]{ K::Q, K::A, K::W, K::S, K::D, K::R, K::F, K::T, K::G, K::Y, K::H, K::J, K::I, K::K, K::O, K::L, U    };
        scaleData[Scale::D]      = new K::Key[C]{ K::A, K::W, K::S, K::D, K::R, K::F, K::T, K::G, K::Y, K::H, K::J, K::I, K::K, K::O, K::L, U,    U    };
        scaleData[Scale::Dsharp] = new K::Key[C]{ K::Q, K::A, K::S, K::E, K::D, K::R, K::F, K::T, K::G, K::H, K::U, K::J, K::I, K::K, K::L, K::P, U    };
        scaleData[Scale::E]      = new K::Key[C]{ K::A, K::S, K::E, K::D, K::R, K::F, K::T, K::G, K::H, K::U, K::J, K::I, K::K, K::L, K::P, U,    U    };
        scaleData[Scale::F]      = new K::Key[C]{ K::A, K::W, K::S, K::E, K::D, K::R, K::F, K::G, K::Y, K::H, K::U, K::J, K::K, K::O, K::L, K::P, U    };
        scaleData[Scale::Fsharp] = new K::Key[C]{ K::Q, K::A, K::W, K::S, K::E, K::D, K::F, K::T, K::G, K::Y, K::H, K::J, K::I, K::K, K::O, K::L, K::P };
        scaleData[Scale::G]      = new K::Key[C]{ K::A, K::W, K::S, K::E, K::D, K::F, K::T, K::G, K::Y, K::H, K::J, K::I, K::K, K::O, K::L, K::P, U    };
        scaleData[Scale::Gsharp] = new K::Key[C]{ K::Q, K::A, K::W, K::S, K::D, K::R, K::F, K::T, K::G, K::H, K::U, K::J, K::I, K::K, K::O, K::L, K::P };
        scaleData[Scale::A]      = new K::Key[C]{ K::A, K::W, K::S, K::D, K::R, K::F, K::T, K::G, K::H, K::U, K::J, K::I, K::K, K::O, K::L, K::P, U    };
        scaleData[Scale::Asharp] = new K::Key[C]{ K::Q, K::A, K::S, K::E, K::D, K::R, K::F, K::G, K::Y, K::H, K::U, K::J, K::I, K::K, K::L, K::P, U    };
        scaleData[Scale::B]      = new K::Key[C]{ K::A, K::S, K::E, K::D, K::R, K::F, K::G, K::Y, K::H, K::U, K::J, K::I, K::K, K::L, K::P, U,    U    };
    }
}

KeyboardController::~KeyboardController() {
    connected = false;
    t_listen.join();

    delete[] keys;
}

void KeyboardController::_pushEvent(ControllerEvent e) {
    if (!isActive()) return;
    events.push_back(e);
    while (events.size() > QUEUE_LIMIT) {
        events.erase(events.begin());
    }
    #ifdef LOG_CONTROLLER_EVENTS
        Log::log("Keyboard Controller Event: ", e.str());
    #endif
}