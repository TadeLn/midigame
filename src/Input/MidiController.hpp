#pragma once


#include <thread>

#include "Midi/MidiHandler.hpp"
#include "Input/Controller.hpp"


#ifndef CONTROLLER_MIDI_SLEEP_TIME
#define CONTROLLER_MIDI_SLEEP_TIME 10
#endif

#ifndef QUEUE_LIMIT
#define QUEUE_LIMIT 1000
#endif


class MidiController : public Controller {

public:
    bool isPressed(unsigned char);

    unsigned char getMidiChannel();
    void setMidiChannel(unsigned char);

    std::string getMidiPort();
    void setMidiPort(unsigned int);

    static void listen(MidiController*);

    MidiController(MidiHandler*, unsigned int, unsigned char);
    ~MidiController();

    void _pushEvent(ControllerEvent);

private:
    bool connected;
    bool* keys;
    std::thread t_listen;
    
    unsigned char midiChannel;
    std::string midiPortName;
    MidiHandler* midi;

};