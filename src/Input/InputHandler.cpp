#include <vector>

#include "Input/Controller.hpp"
#include "Input/InputHandler.hpp"


unsigned int InputHandler::getControllerCount() {
    return controllers.size();
}

Controller* InputHandler::getController(unsigned int id) {
    return controllers[id];
}

void InputHandler::connectController(Controller* ptr) {
    controllers.push_back(ptr);
}

void InputHandler::disconnectController(unsigned int id) {
    delete controllers[id];
    controllers.erase(controllers.begin() + id);
}

InputHandler::~InputHandler() {
    for (unsigned int i = 0; i < controllers.size(); i++) {
        disconnectController(i);
    }
}