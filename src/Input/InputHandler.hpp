#pragma once


#include <vector>

#include "Input/Controller.hpp"


class InputHandler {

public:
    unsigned int getControllerCount();
    Controller* getController(unsigned int);
    void connectController(Controller*);
    void disconnectController(unsigned int);

    ~InputHandler();

private:
    std::vector<Controller*> controllers;

};