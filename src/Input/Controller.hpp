#pragma once


#include <vector>

#include "Input/ControllerEvent.hpp"
#include "Utilities/Scale.hpp"


class Controller {

public:
    enum Type {
        CONTROLLER_UNSET,
        CONTROLLER_JOY,
        CONTROLLER_KEYBOARD,
        CONTROLLER_MIDI
    };

    bool isActive();
    void activate();
    void deactivate();

    Type getType();
    virtual bool isPressed(unsigned char);

    bool pollEvent(ControllerEvent*);
    void dropEvents();
    
    char transpose;

    int scale;

    Controller();
    ~Controller();

protected:
    bool active;
    Type type;
    std::vector<ControllerEvent> events;

};