#include <chrono>
#include <string>

#include "Utilities/Error.hpp"
#include "Input/ControllerEvent.hpp"


std::string ControllerEvent::str() {
    return std::string("t: " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(timestamp.time_since_epoch()).count()) + " T: " + std::to_string((unsigned int)type) + " K: " + std::to_string((unsigned int)key) + " V: " + std::to_string((unsigned int)vel));
}

ControllerEvent::ControllerEvent()
    : ControllerEvent(EVENT_UNKNOWN)
{}

ControllerEvent::ControllerEvent(Type type)
    : ControllerEvent(type, 0, 127, std::chrono::system_clock::now())
{}

ControllerEvent::ControllerEvent(Type type, unsigned char key, unsigned char vel, std::chrono::system_clock::time_point timestamp) {
    this->timestamp = timestamp;

    if (vel > 127) {
        Error::throwErr(HERE, "Velocity cannot be above 127!");
    }
    this->type = type;
    this->key = key;
    this->vel = vel;
    
    if (vel == 0) {
        this->type = EVENT_RELEASE;
    }
    if (type == EVENT_RELEASE) {
        this->vel = 0;
    }
}