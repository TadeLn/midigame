#include <csignal>

#include "Global.hpp"
#include "Game/Game.hpp"
#include "Utilities/Log.hpp"
#include "Utilities/Error.hpp"


int main(int argc, char* argv[]) {
    signal(SIGINT, Global::interrupt);
    
    Game game;

    try {
        game.start();
        Log::log("Exited succesfully");
    } catch (Error* e) {
        e->print();
    }
}
