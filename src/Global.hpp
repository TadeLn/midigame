#pragma once

#include <chrono>

#include "Utilities/Log.hpp"


namespace Global {

std::chrono::system_clock::time_point getStartTimestamp();
void setStartTimestamp();
bool isStopped();
void halt();
void interrupt(int);

};