#include "Global.hpp"

#include "Utilities/Log.hpp"


namespace Global {

namespace {
    std::chrono::system_clock::time_point startTimestamp;
    bool timestampSet = false;
    bool stop = false;
}

std::chrono::system_clock::time_point getStartTimestamp() {
    return startTimestamp;
}

void setStartTimestamp() {
    if (!timestampSet) {
        startTimestamp = std::chrono::system_clock::now();
        timestampSet = true;
    }
}

bool isStopped() {
    return stop;
}

void halt() {
    stop = true;
}

void interrupt(int signum) {
    Log::log("Interrupt signal (", signum, ") received.\n");
    halt();
}

};