#include "Render/RenderObject.hpp"

#include <SFML/Graphics.hpp>

#include "Render/RenderNote.hpp"


sf::Vector2i RenderObject::getPos() {
    return sf::Vector2i(0, 0);
}

sf::Vector2f RenderObject::getSize() {
    return sf::Vector2f(10, 10);
}

sf::Color RenderObject::getColor() {
    return sf::Color::White;
}

unsigned int RenderObject::getTime() {
    return time;
}

unsigned int RenderObject::getUnloadTime() {
    return unloadTime;
}

RenderObject::RenderObject() {
    time = 0;
    unloadTime = 0;
}

RenderObject* RenderObject::createFromNote(Note note, Song* songPtr) {
    RenderObject* result = nullptr;

    if (note.type == Note::NOTE_SHORT) {
        result = new RenderNote(note.key, songPtr->beatToOffset(note.timing));
    }
    
    return result;
}
