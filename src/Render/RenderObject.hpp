#pragma once

#include <SFML/Graphics.hpp>

#include "Song/Song.hpp"
#include "Song/Note.hpp"


class RenderObject {

public:
    virtual sf::Vector2i getPos();
    virtual sf::Vector2f getSize();
    virtual sf::Color getColor();
    unsigned int getTime();
    unsigned int getUnloadTime();
    static RenderObject* createFromNote(Note, Song*);

    RenderObject();

protected:
    unsigned int time;
    unsigned int unloadTime;

};