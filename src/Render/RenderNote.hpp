#pragma once

#include <SFML/Graphics.hpp>

#include "RenderObject.hpp"


class RenderNote : public RenderObject {

public:
    sf::Vector2i getPos();
    sf::Vector2f getSize();
    sf::Color getColor();
    unsigned int getTime();
    
    RenderNote(unsigned char, unsigned int);

private:
    unsigned char key;


};