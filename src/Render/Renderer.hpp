#pragma once

#include <thread>
#include <vector>
#include <SFML/Window.hpp>

#include "Render/RenderObject.hpp"
#include "Game/Game.hpp"

class Game;


class Renderer {

public:
    void start();
    void stop();

    void addRenderObject(RenderObject*);

    bool isWorking();

    static void update(Renderer*);

    Renderer(Game*);
    ~Renderer();
    
    sf::RenderWindow* win;
    Game* g;

private:
    bool working;
    std::thread t_update;
    std::vector<RenderObject*> renderObjects;

    int noteLinePosition;
    float noteSpeed;

    sf::Font defaultFont;

};