#include "Render/RenderNote.hpp"

#include <SFML/Graphics.hpp>

#include "Song/Note.hpp"
#include "Song/Song.hpp"


sf::Vector2i RenderNote::getPos() {
    return sf::Vector2i(key * 50, -time);
}

sf::Vector2f RenderNote::getSize() {
    return sf::Vector2f(50, 30);
}

sf::Color RenderNote::getColor() {
    return sf::Color::Green;
}

RenderNote::RenderNote(unsigned char key, unsigned int offset) {
    this->key = key;
    this->time = offset;
}