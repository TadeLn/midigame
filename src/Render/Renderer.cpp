#include <SFML/Graphics.hpp>

#include "Game/Game.hpp"
#include "Render/Renderer.hpp"
#include "Utilities/Error.hpp"
#include "Utilities/Log.hpp"


void Renderer::start() {
    working = true;
    win->setActive(false);
    t_update = std::thread(update, this);
}

void Renderer::stop() {
    win->close();

    working = false;
    t_update.join();
}

void Renderer::addRenderObject(RenderObject* renderObject) {
    renderObjects.push_back(renderObject);
}

bool Renderer::isWorking() { return working; }

void Renderer::update(Renderer* r) {
    // Set active
    r->win->setActive(true);

    // Variables for calculating time elapsed in an iteration
    double deltaTime = 0;
    std::chrono::high_resolution_clock::time_point startTime;

    // Settings
    r->noteLinePosition = 500;
    r->noteSpeed = 0.2;

    // Load font
    if (!r->defaultFont.loadFromFile("res/roboto/Roboto-Light.ttf")) {
        Error::throwErr(HERE, "Couldn't load font Roboto");
    }

    // Define shapes to render
    sf::RectangleShape noteRectangle;
    sf::RectangleShape noteHitLineRectangle;
    sf::Text scoreText;

    // Set up shapes to render
    noteHitLineRectangle.setPosition(0, r->noteLinePosition);
    noteHitLineRectangle.setSize(sf::Vector2f(r->win->getSize().x, 5));
    noteHitLineRectangle.setFillColor(sf::Color::White);
    scoreText.setFont(r->defaultFont);
    scoreText.setCharacterSize(50);


    while (r->isWorking()) {
        // Calculate time since last frame
        deltaTime = std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now() - startTime).count();
        startTime = std::chrono::high_resolution_clock::now();


        // Check for events
        sf::Event e;
        while (r->win->pollEvent(e)) {
            switch (e.type) {

                case sf::Event::Closed:
                    r->win->close();
                    break;

                case sf::Event::KeyPressed:
                case sf::Event::KeyReleased:
                    r->g->_pushKeyboardEvent(e);
                    break;
            }
        }

        // Check if window is open
        r->working = r->win->isOpen();
        if (!r->isWorking()) break;

        // Draw
        r->win->clear();

        // Draw note
        for (unsigned int i = 0; i < r->renderObjects.size(); i++) {
            noteRectangle.setSize(r->renderObjects[i]->getSize());
            sf::Vector2i pos = r->renderObjects[i]->getPos();
            noteRectangle.setPosition(
                pos.x,
                (pos.y * r->noteSpeed) + (r->g->getCurrentSongTime() * r->noteSpeed) + r->noteLinePosition - r->renderObjects[i]->getSize().y
            );
            noteRectangle.setFillColor(r->renderObjects[i]->getColor());

            r->win->draw(noteRectangle);
        }

        // Draw note hit line
        r->win->draw(noteHitLineRectangle);

        // Update and draw score
        scoreText.setString(std::string("Score: ") + std::to_string(r->g->getScore()));
        scoreText.setPosition((r->win->getSize().x / 2) - (scoreText.getLocalBounds().width / 2), 10);
        r->win->draw(scoreText);

        r->win->display();
    }
}

Renderer::Renderer(Game* gamePtr) {
    g = gamePtr;
    win = new sf::RenderWindow(sf::VideoMode(800, 600), "Window");
    win->setActive(false);
}

Renderer::~Renderer() {
    if (working) {
        stop();
    }
}
