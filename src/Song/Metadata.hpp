#pragma once

#include <string>


// A class used for holding basic metadata of songs
class Metadata {

public:
    std::string title;
    std::string author;
    std::string album;
    std::string mapper; // The creator of the map
    unsigned int year;
    std::string audioFilename; // filename of the audio file

    Metadata();
    Metadata(std::string, std::string, std::string, std::string, unsigned int, std::string);

};