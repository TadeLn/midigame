#pragma once

#include <string>
#include <fstream>
#include <optional>
#include <vector>

#include "Utilities/Error.hpp"
#include "Utilities/Log.hpp"
#include "Song/Metadata.hpp"
#include "Song/TimingSection.hpp"
#include "Song/Effect.hpp"
#include "Song/SongEvent.hpp"
#include "Song/DifficultyTrack.hpp"



// A class used for holding song data
class Song {

public:



    Metadata metadata; // Song's metadata (title, author, mapper etc.)
    std::vector<TimingSection> timingSections; // Timing sections
    std::vector<Effect> effects; // Global effects
    std::vector<SongEvent> events; // Global events
    std::vector<DifficultyTrack> difficulties; // All difficulties
    unsigned int resolution; // Time "resolution", precision of beats; default 768

    unsigned int beatToOffset(BeatTiming);
    BeatTiming offsetToBeat(unsigned int);

    void loadFromString(std::string);
    void loadFromFile(std::string);

    std::string str();

    Song();

};