#include "Song/Note.hpp"

#include "Song/BeatTiming.hpp"


Note::Note()
    : Note(NOTE_SHORT, 0, BeatTiming())
{}

Note::Note(Note::Type type, unsigned char key, BeatTiming timing) {
    this->type = type;
    this->key = key;
    this->timing = timing;

    this->start = true;
    this->end = false;
}