#pragma once

#include <optional>


// A timing section of the song
class TimingSection {

public:
    unsigned int offset; // Time of the timing section (ms)
    std::optional<double> bpm; // Tempo value
    std::optional<unsigned char> signatureT; // Top value of a time signature
    std::optional<unsigned char> signatureB; // Bottom value of a time signature

};