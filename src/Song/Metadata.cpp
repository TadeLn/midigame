#include "Song/Metadata.hpp"

#include <string>


Metadata::Metadata()
    : Metadata("", "", "", "", 0, "")
{}

Metadata::Metadata(std::string title, std::string author, std::string album, std::string mapper, unsigned int year, std::string audioFilename) {
    this->title = title;
    this->author = author;
    this->album = album;
    this->mapper = mapper;
    this->year = year;
    this->audioFilename = audioFilename;
}