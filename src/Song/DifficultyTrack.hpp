#pragma once

#include <vector>
#include <string>

#include "Song/SongEvent.hpp"
#include "Song/Note.hpp"


// A class that holds notes for a difficulty
class DifficultyTrack {

public:
    std::string type; // Type of the track
    unsigned char level; // Level of the difficulty
    std::vector<SongEvent> events; // Local events
    std::vector<Note> notes; // Notes of the difficulty

};