#pragma once

#include <optional>

#include "Song/BeatTiming.hpp"


// A class used for holding note data
class Note {

public:
    enum Type {
        NOTE_SHORT,
        NOTE_SUSTAIN,
        NOTE_TRILL,
        NOTE_MINE,
        NOTE_VIBRATO,
        NOTE_SLIDE
    };

    BeatTiming timing; // Timing of the note
    Type type; // Type of the note
    unsigned char key; // Pitch of the note
    bool start; // Does the sustain have a NOTE_ON check at the start?
    bool end; // Does the sustain have a NOTE_OFF check at the end?
    std::optional<unsigned int> length; // Note length in beat units 
    std::optional<unsigned char> keyEnd; // Slide end pitch
    std::optional<unsigned int> color; // RGBA color of the note
    std::optional<unsigned int> effectId; // Effect activated on note hit

    Note();
    Note(Type, unsigned char, BeatTiming);

};