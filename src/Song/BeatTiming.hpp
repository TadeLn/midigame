#pragma once


// A class used to time beats
class BeatTiming {

public:
    unsigned int section; // Timing Section Number (new timing sections are created by BPM or Time Signature change)
    unsigned int units; // Every beat = 768 units by deafult, this value can be changed per song

    BeatTiming();
    BeatTiming(unsigned int, unsigned int);

};