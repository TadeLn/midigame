#pragma once

#include "Song/BeatTiming.hpp"


// A class that stores various events
class SongEvent {

public:
    BeatTiming timing; // Timing of the event

};