#include "Song/BeatTiming.hpp"


BeatTiming::BeatTiming()
    : BeatTiming(0, 0)
{}

BeatTiming::BeatTiming(unsigned int section, unsigned int units) {
    this->section = section;
    this->units = units;
}