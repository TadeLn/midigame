#include "Song/Song.hpp"

#include <string>
#include <fstream>
#include <optional>
#include <rapidjson/document.h>

#include "Song/BeatTiming.hpp"
#include "Song/Metadata.hpp"
#include "Song/TimingSection.hpp"


#define DEFAULT_BPM 120.f


// Convert beat timing to milliseconds
unsigned int Song::beatToOffset(BeatTiming timing) {
    unsigned int initialOffset = timingSections[timing.section].offset;

    std::optional<double> bpm;

    // Search for the bpm
    for (int i = timing.section; i >= 0; i--) {
        if (timingSections[i].bpm.has_value()) {
            bpm = timingSections[i].bpm.value();
            break;
        }
    }

    /*
        bpm / 60 // Beats Per Second
        60 / bpm // Seconds Per Beat
        60000 / bpm // Milliseconds Per Beat

        // 1 Beat = 768 units
        (60000 / bpm) / 768 // Milliseconds Per Unit
    */

    unsigned int beatOffset = (double)timing.units * (double)((60000.f / bpm.value_or(120.f)) / resolution);

    return initialOffset + beatOffset;
}

// Convert milliseconds to beat timing
BeatTiming Song::offsetToBeat(unsigned int offset) {
    unsigned int sectionNumber;
    for (sectionNumber = timingSections.size() - 1; sectionNumber >= 0; sectionNumber--) {
        if (timingSections[sectionNumber].offset <= offset) break; 
    }

    std::optional<double> bpm;

    // Search for the bpm
    for (unsigned int i = sectionNumber; i >= 0; i--) {
        if (timingSections[i].bpm.has_value()) {
            bpm = timingSections[i].bpm.value();
            break;
        }
    }

    // Offset relative to timing section
    unsigned int relativeOffset = offset - timingSections[sectionNumber].offset;

    /*
        bpm / 60 // Beats Per Second
        bpm / 60000 // Beats Per Millisecond
        (bpm / 60000) * 768 // Units Per Millisecond
    */

    unsigned int units = (double)((bpm.value_or(DEFAULT_BPM) / 60000.f) * resolution) * relativeOffset;
    return BeatTiming(sectionNumber, units);
}

void Song::loadFromFile(std::string filename) {
    std::ifstream file;
    file.open(filename);
    if (!file.is_open()) {
        Error::throwErr(HERE, "File '" + filename + "' couldn't be opened");
    }
    std::string fileContents;
    std::string line;
    while (getline(file, line)) {
        fileContents += line;
    }
    file.close();
    loadFromString(fileContents);
}

void Song::loadFromString(std::string JSONstring) {
    rapidjson::Document document;
    document.Parse(JSONstring.c_str());

    // /metadata
    if (document.HasMember("metadata") && document["metadata"].IsObject()) {
        auto metadata = document["metadata"].GetObject();

        // /metadata/title
        if (metadata.HasMember("title") && metadata["title"].IsString()) {
            this->metadata.title = metadata["title"].GetString();
        }

        // /metadata/author
        if (metadata.HasMember("author") && metadata["author"].IsString()) {
            this->metadata.author = metadata["author"].GetString();
        }

        // /metadata/album
        if (metadata.HasMember("album") && metadata["album"].IsString()) {
            this->metadata.album = metadata["album"].GetString();
        }

        // /metadata/mapper
        if (metadata.HasMember("mapper") && metadata["mapper"].IsString()) {
            this->metadata.mapper = metadata["mapper"].GetString();
        }

        // /metadata/year
        if (metadata.HasMember("year") && metadata["year"].IsInt()) {
            this->metadata.year = metadata["year"].GetInt();
        }

        // /metadata/audio
        if (metadata.HasMember("audio") && metadata["audio"].IsString()) {
            this->metadata.audioFilename = metadata["audio"].GetString();
        }
    }

    // /timing
    if (document.HasMember("timing") && document["timing"].IsObject()) {
        auto timing = document["timing"].GetObject();

        // /timing/resolution
        if (timing.HasMember("resolution") && timing["resolution"].IsUint()) {
            this->resolution = timing["resolution"].GetUint();
        }

        // /timing/sections
        if (timing.HasMember("sections") && timing["sections"].IsArray()) {
            auto sections = timing["sections"].GetArray();

            for (rapidjson::SizeType i = 0; i < sections.Size(); i++) {

                // /timing/sections/i
                if (sections[i].IsObject()) {
                    auto section = sections[i].GetObject();
                    TimingSection result;

                    // /timing/sections/i/offset
                    if (section.HasMember("offset") && section["offset"].IsUint()) {
                        result.offset = section["offset"].GetUint();
                    }

                    // /timing/sections/i/bpm
                    if (section.HasMember("bpm") && section["bpm"].IsDouble()) {
                        result.bpm = section["bpm"].GetDouble();
                    } else if (section.HasMember("bpm") && section["bpm"].IsUint()) {
                        result.bpm = (double)section["bpm"].GetUint();
                    }

                    // /timing/sections/i/signature
                    if (section.HasMember("signature") && section["signature"].IsArray()) {
                        auto array = section["signature"].GetArray();

                        if (array.Size() == 2) {
                            // /timing/sections/i/signature/0
                            if (array[0].IsUint()) {
                                result.signatureT = array[0].GetUint();
                            }

                            // /timing/sections/i/signature/1
                            if (array[1].IsUint()) {
                                result.signatureB = array[1].GetUint();
                            }
                        }
                    }

                    this->timingSections.push_back(result);
                }
            }
        }
    }
    
    // /difficulties
    if (document.HasMember("difficulties") && document["difficulties"].IsArray()) {
        auto difficulties = document["difficulties"].GetArray();

        for (rapidjson::SizeType i = 0; i < difficulties.Size(); i++) {

            // /difficulties/i
            if (difficulties[i].IsObject()) {
                auto difficulty = difficulties[i].GetObject();
                DifficultyTrack result;

                // /difficulties/i/type
                if (difficulty.HasMember("type") && difficulty["type"].IsString()) {
                    result.type = difficulty["type"].GetString();
                }

                // /difficulties/i/level
                if (difficulty.HasMember("level") && difficulty["level"].GetUint()) {
                    result.level = difficulty["level"].GetUint();
                }
                
                // /difficulties/i/notes
                if (difficulty.HasMember("notes") && difficulty["notes"].IsArray()) {
                    auto notes = difficulty["notes"].GetArray();

                    for (rapidjson::SizeType j = 0; j < notes.Size(); j++) {

                        // /difficulties/i/notes/j
                        if (notes[j].IsObject()) {
                            auto note = notes[j].GetObject();
                            Note resultNote;

                            // /difficulties/i/notes/j/type
                            if (note.HasMember("type") && note["type"].IsUint()) {
                                resultNote.type = (Note::Type)note["type"].GetUint();
                            }

                            // /difficulties/i/notes/j/key
                            if (note.HasMember("key") && note["key"].IsUint()) {
                                resultNote.key = note["key"].GetUint();
                            }

                            // /difficulties/i/notes/j/time
                            if (note.HasMember("time") && note["time"].IsObject()) {
                                auto timing = note["time"].GetObject();
                                BeatTiming resultTiming;

                                // /difficulties/i/notes/j/time/s
                                if (timing.HasMember("s") && timing["s"].IsUint()) {
                                    resultTiming.section = timing["s"].GetUint();
                                }

                                // /difficulties/i/notes/j/time/b
                                if (timing.HasMember("b") && timing["b"].IsUint()) {
                                    resultTiming.units = timing["b"].GetUint();
                                }

                                resultNote.timing = resultTiming;
                            }

                            result.notes.push_back(resultNote);
                        }
                    }
                }
                
                // /difficulties/i/events
                if (difficulty.HasMember("events") && difficulty["events"].IsArray()) {
                    
                }

                this->difficulties.push_back(result);
            }
        }
    }
}

Song::Song() {
    resolution = 768;
}

std::string Song::str() {
    return std::string();
}