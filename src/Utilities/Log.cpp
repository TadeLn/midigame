#include "Log.hpp"

#include <sstream>

#include "Utilities/Settings.hpp"


namespace Log {

void updateSettings() {
    // Don't check settings too often (to avoid lag)
    if (lastCheckedSettings - std::chrono::steady_clock::now() < std::chrono::seconds(1)) return;
    
    logFile = Settings::getSetting_long("logFile") > 0;
    logConsole = Settings::getSetting_long("logConsole") > 0;
}

std::stringstream makeSS() {
    std::stringstream ss;
    ss << '\n';
    return ss;
}

};