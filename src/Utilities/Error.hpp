#pragma once

#include <string>
#include <vector>

#include "Utilities/ErrorPath.hpp"


class Error {

public:

    static void throwErr(ErrorPath, std::string);
    static void throwErr(ErrorPath, std::string, unsigned int);
    void passErr(ErrorPath);

    std::string str();

    unsigned int getErrorCode();
    std::string getMessage();
    void push(ErrorPath);

    void print();

    Error(ErrorPath, std::string, unsigned int);

private:
    unsigned int errorCode;
    std::string message;
    std::vector<ErrorPath> pathStack;

};