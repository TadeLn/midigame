#pragma once

#include <string>


#define HERE ErrorPath(std::string(__FILE__), __LINE__, std::string(__FUNCTION__))


class ErrorPath {

public:
    std::string str();

    std::string getFile();
    unsigned int getLine();
    std::string getFunction();

    ErrorPath(std::string, unsigned int, std::string);

private:
    std::string file;
    unsigned int line;
    std::string function;

};