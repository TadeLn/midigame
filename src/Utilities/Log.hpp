#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <chrono>


namespace Log {

namespace {
    std::chrono::steady_clock::time_point lastCheckedSettings;
    bool logConsole = 1;
    bool logFile = 0;
}

void updateSettings();

std::stringstream makeSS();

template<typename First, typename ... Rest>
std::stringstream makeSS(First first, Rest... rest) {
    std::stringstream ss;
    ss << first << makeSS(rest...).str();
    return ss;
}

template<typename ... Args>
void log(Args... args) {
    std::stringstream ss = makeSS(args...);

    updateSettings();

    if (logConsole) {
        std::cout << ss.str();
    }
    if (logFile) {
        std::string filename = "game.log";
        
        std::ofstream file(filename, std::ios::app);
        file << ss.str();
    }
}

};