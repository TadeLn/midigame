#include "Utilities/ErrorPath.hpp"

#include <string>


std::string ErrorPath::str() {
    return std::string(file + ":" + std::to_string(line) + " : " + function);
}

std::string ErrorPath::getFile() {
    return file;
}

unsigned int ErrorPath::getLine() {
    return line;
}

std::string ErrorPath::getFunction() {
    return function;
}

ErrorPath::ErrorPath(std::string file, unsigned int line, std::string function) {
    this->file = file;
    this->line = line;
    this->function = function;
}