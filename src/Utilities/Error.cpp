#include "Utilities/Error.hpp"

#include <iostream>
#include <string>

#include "Utilities/ErrorPath.hpp"


void Error::throwErr(ErrorPath path, std::string message) {
    Error::throwErr(path, message, 1);
}

void Error::throwErr(ErrorPath path, std::string message, unsigned int errorCode) {
    throw new Error(path, message, errorCode);
}

void Error::passErr(ErrorPath path) {
    push(path);
    throw this;
}

std::string Error::str() {
    std::string result;
    if (errorCode >> 31) { // if the highest bit is set, show a warning instead of an error
        result += "\n\u001b[35;1mWarning: \u001b[37;1m" + message + "\u001b[0m\n";
    } else {
        result += "\n\u001b[31;1mError: \u001b[37;1m" + message + "\u001b[0m\n";

    }
    for (unsigned int i = 0; i < pathStack.size(); i++) {
        result += "  in: " + pathStack[i].str() + "\n";
    }
    return result;
}

unsigned int Error::getErrorCode() {
    return errorCode;
}

std::string Error::getMessage() {
    return message;
}

void Error::push(ErrorPath path) {
    pathStack.push_back(path);
}

void Error::print() {
    std::cerr << str() << "\n";
}

Error::Error(ErrorPath path, std::string message, unsigned int errorCode) {
    this->push(path);
    this->message = message;
    this->errorCode = errorCode;
}