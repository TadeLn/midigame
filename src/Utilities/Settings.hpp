#pragma once


#include <vector>
#include <utility>
#include <string>


/*

All settings:


*/


namespace Settings {

namespace {
    std::vector<std::pair<std::string, std::string>> settings;
};

std::string getSetting_string(std::string);
std::string getSetting_string(std::string, std::string);
long getSetting_long(std::string);
long getSetting_long(std::string, long);
unsigned long getSetting_ulong(std::string);
unsigned long getSetting_ulong(std::string, unsigned long);
double getSetting_double(std::string);
double getSetting_double(std::string, double);

void setSetting(std::string setting, std::string value);
void setSetting(std::string setting, long value);
void setSetting(std::string setting, unsigned long value);
void setSetting(std::string setting, double value);

};