#pragma once


enum Scale {
    Unknown = -1,
    C = 0,
    Csharp,
    D,
    Dsharp,
    E,
    F,
    Fsharp,
    G,
    Gsharp,
    A,
    Asharp,
    B,
    _SIZE_
};