#include "Utilities/Settings.hpp"

#include <vector>
#include <utility>
#include <string>

#include "Utilities/Log.hpp"


namespace Settings {

std::string getSetting_string(std::string setting) {
    for (int i = 0; i < settings.size(); i++) {
        if (settings[i].first == setting) {
            return settings[i].second;
        }
    }
    // If no setting is found, add a new setting, with an empty value
    setSetting(setting, "");
    return std::string("");
}

long getSetting_long(std::string setting) {
    return std::stol(getSetting_string(setting));
}

long getSetting_long(std::string setting, long defaultValue) {
    try {
        return getSetting_long(setting);
    } catch (std::exception& e) {
        Log::log(e.what());
        return defaultValue;
    }
}

unsigned long getSetting_ulong(std::string setting) {
    return std::stoul(getSetting_string(setting));
}

unsigned long getSetting_ulong(std::string setting, unsigned long defaultValue) {
    try {
        return getSetting_ulong(setting);
    } catch (std::exception& e) {
        Log::log(e.what());
        return defaultValue;
    }
}

double getSetting_double(std::string setting) {
    return std::stod(getSetting_string(setting));
}

double getSetting_double(std::string setting, double defaultValue) {
    try {
        return getSetting_double(setting);
    } catch (std::exception& e) {
        Log::log(e.what());
        return defaultValue;
    }
}


void setSetting(std::string setting, std::string value) {
    for (int i = 0; i < settings.size(); i++) {
        if (settings[i].first == setting) {
            settings[i].second = value;
            return;
        }
    }
    settings.push_back(std::pair<std::string, std::string>(setting, value));
}

void setSetting(std::string setting, int value) {
    setSetting(setting, std::to_string(value));
}

void setSetting(std::string setting, unsigned int value) {
    setSetting(setting, std::to_string(value));
}

void setSetting(std::string setting, double value) {
    setSetting(setting, std::to_string(value));
}

};