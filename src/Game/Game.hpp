#pragma once

#include <chrono>
#include <vector>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>

#include "Midi/MidiHandler.hpp"
#include "Input/InputHandler.hpp"
#include "Song/Song.hpp"
#include "Game/HitObject.hpp"
#include "Render/Renderer.hpp"
#include "Utilities/Error.hpp"


class Game {

public:
    enum Screen {
        SCREEN_MAIN_MENU,
        SCREEN_SONG_SELECT,
        SCREEN_SONG,
        SCREEN_SONG_RESULTS
    };

    void _pushKeyboardEvent(sf::Event);
    std::vector<bool> getKeyboard();

    bool isPlaying();
    int getCurrentSongTime(std::chrono::system_clock::time_point);
    int getCurrentSongTime();
    unsigned int getScore();
    static std::chrono::system_clock::time_point getStartTime();

    void changeScreen(Screen);
    Screen getScreen();

    void reloadSong(unsigned int);
    void loadSongFromFile(std::string);

    void start();

    Game();
    ~Game();

private:
    MidiHandler midiHandler;
    InputHandler inputHandler;
    Renderer* r;

    Song* currentSong;
    std::string currentSongPath;
    sf::Music songMusic;

    bool playing;
    unsigned int score;

    std::vector<HitObject*> loadedHitObjects;
    DifficultyTrack currentTrack;

    int objToLoadIndex;
    int hitIndex;
    int lEventToLoadIndex;
    int gEventToLoadIndex;

    std::chrono::system_clock::time_point songStartTimestamp;

    std::vector<bool> keyboardState;
    std::vector<bool> keyboardState_old;

};