#include "Game/Game.hpp"

#include <math.h>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>

#include "Global.hpp"
#include "Input/ControllerEvent.hpp"
#include "Input/InputHandler.hpp"
#include "Input/KeyboardController.hpp"
#include "Input/MidiController.hpp"
#include "Render/Renderer.hpp"
#include "Song/All.hpp"
#include "Utilities/Log.hpp"
#include "Utilities/Error.hpp"
#include "Utilities/Scale.hpp"


#define TRANSPOSE 46
#define FILTER_CHANNEL 0
#define SOUND_FILENAME "res/megalo.wav"

#define PLAY_SOUNDS
#define IGNORE_VELOCITY

#define HIT_WINDOW_BAD 300
#define HIT_WINDOW_GOOD 100
#define HIT_WINDOW_PERFECT 50

#define HIT_SCORE_BAD 100
#define HIT_SCORE_GOOD 300
#define HIT_SCORE_PERFECT 500



void Game::_pushKeyboardEvent(sf::Event e) {
    if (e.key.code < 0) return; // Some keys (i.e. CapsLock) have key code of -1 (which breaks further processing)

    while (keyboardState.size() <= e.key.code) {
        keyboardState.push_back(false);
    }
    if (e.type == sf::Event::KeyPressed) {
        keyboardState[e.key.code] = true;
    } else if (e.type == sf::Event::KeyReleased) {
        keyboardState[e.key.code] = false;
    }
}

std::vector<bool> Game::getKeyboard() {
    return keyboardState;
}

int Game::getCurrentSongTime(std::chrono::system_clock::time_point when) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(when - songStartTimestamp).count();
}

int Game::getCurrentSongTime() {
    return getCurrentSongTime(std::chrono::system_clock::now());
}

unsigned int Game::getScore() {
    return score;
}

void Game::reloadSong(unsigned int songId) {
    // Load a song
    currentSong = new Song();
    try {
        loadSongFromFile("res/songs/" + std::to_string(songId) + "/");
    } catch (Error* e) {
        e->passErr(HERE);
    }

    // Reset values
    currentTrack = currentSong->difficulties[0];
    hitIndex = 0;
    objToLoadIndex = 0;
    score = 0;

    // Start the song
    songStartTimestamp = std::chrono::system_clock::now() + std::chrono::seconds(1);
}

void Game::loadSongFromFile(std::string filePath) {
    currentSong->loadFromFile(filePath + "song.json");
    currentSongPath = filePath;
    songMusic.stop();
    
    try {
        if (!songMusic.openFromFile(filePath + currentSong->metadata.audioFilename)) {
            Error::throwErr(HERE, "File " + std::string(filePath + currentSong->metadata.audioFilename) + " couldn't be opened", -1);
        }
    } catch (Error* e) {
        e->print();
    }
}

void Game::start() {
    Global::setStartTimestamp();

    // Initialize renderer
    r = new Renderer(this);
    r->start();

    // Initialize MIDI handler
    try {
        midiHandler.initializeIn();
    } catch (Error* e) {
        e->passErr(HERE);
    }

    Controller* i_keyboard = new KeyboardController(this);
    inputHandler.connectController(i_keyboard);

    // Check available ports vs. specified.
    unsigned int port = 0;
    unsigned int nPorts = midiHandler.getPortCount();
    std::string portName;
    std::cout << "\nThere are " << nPorts << " MIDI ports avaliable:\n";
    if (nPorts == 0) {
        std::cout << "No output ports available!\n";
        return;
    } else {
        for (unsigned int i = 0; i < nPorts; i++) {
            portName = midiHandler.getPortName(i);
            std::cout << "  #" << i << ": " << portName << "\n";
        }

        if (nPorts > 1) {
            do {
                std::cout << "\nChoose a port number: ";
                std::cin >> port;
            } while (port >= nPorts);
        } else {
            port = 0;
        }
    }

    std::cout << "Opening port #" << port << "...\n";

    Controller* midi = new MidiController(&midiHandler, port, FILTER_CHANNEL);
    inputHandler.connectController(midi);

    // Load sounds
    sf::SoundBuffer buffer;
    buffer.loadFromFile(SOUND_FILENAME);

    std::vector<sf::Sound> sounds;

    for (unsigned char i = 0; i < 128; i++) {
        sf::Sound sound;
        sound.setBuffer(buffer);
        sound.setPitch((double)0.03125 * (double)pow(2.f, (float)(i)/12.f) );
        sounds.push_back(sound);
    }

    std::cout << "Reading MIDI from port " << midiHandler.getPortName(port) << "... quit with Ctrl-C.\n";


    const unsigned int controllerNumber = 0;
    const unsigned int loadingMargin = 10000;
    const unsigned int musicSyncMargin = 50;
    
    reloadSong(2);

    int previousSongTime;

    while (!Global::isStopped()) {
        auto now = std::chrono::system_clock::now();

        // Handle key events

        // (Temp) Change scale / change octave
        // down
        if (keyboardState_old[sf::Keyboard::Hyphen] < keyboardState[sf::Keyboard::Hyphen]) {
            if (keyboardState[sf::Keyboard::LShift]) i_keyboard->transpose -= 12;
            else i_keyboard->scale--;

            if (i_keyboard->scale == Scale::A ||
                i_keyboard->scale == Scale::B ||
                i_keyboard->scale == Scale::D ||
                i_keyboard->scale == Scale::E ||
                i_keyboard->scale == Scale::G
            ) i_keyboard->scale--;
        }

        // up
        if (keyboardState_old[sf::Keyboard::Equal] < keyboardState[sf::Keyboard::Equal]) {
            if (keyboardState[sf::Keyboard::LShift]) i_keyboard->transpose += 12;
            else i_keyboard->scale++;

            if (i_keyboard->scale == Scale::A ||
                i_keyboard->scale == Scale::B ||
                i_keyboard->scale == Scale::D ||
                i_keyboard->scale == Scale::E ||
                i_keyboard->scale == Scale::G
            ) i_keyboard->scale++;
        }

        // Check if scale is in bounds, update octave
        if (i_keyboard->scale >= Scale::_SIZE_) {
            i_keyboard->scale = Scale::C;
            i_keyboard->transpose += 12;
        } else if (i_keyboard->scale <= Scale::Unknown) {
            i_keyboard->scale = Scale::Asharp;
            i_keyboard->transpose -= 12;
        }

        // (Temp) Reload song
        if (keyboardState_old[sf::Keyboard::Tilde] < keyboardState[sf::Keyboard::Tilde]) reloadSong(2);

        // Update keyboard state
        keyboardState_old = std::vector<bool>(keyboardState);


        // Detect music start
        if (previousSongTime < 0 && getCurrentSongTime(now) >= 0) {
            songMusic.play();
        } else if (getCurrentSongTime(now) < 0) {
            songMusic.stop();
        }

        // Resync music
        if (getCurrentSongTime(now) >= 0) {
            if (getCurrentSongTime(now) - musicSyncMargin > songMusic.getPlayingOffset().asMilliseconds() ||
                getCurrentSongTime(now) + musicSyncMargin < songMusic.getPlayingOffset().asMilliseconds()) {
                songMusic.setPlayingOffset(sf::milliseconds(getCurrentSongTime(now)));
            }
        }

        // Load hit objects
        while (objToLoadIndex < currentTrack.notes.size() &&
            (int)currentSong->beatToOffset(currentTrack.notes[objToLoadIndex].timing) - (int)loadingMargin < getCurrentSongTime(now)) {
            Log::log("Load #", objToLoadIndex);

            // Create new hit object
            std::vector<HitObject*> newHObjects = HitObject::createFromNote(currentTrack.notes[objToLoadIndex], currentSong);
            for (unsigned int i = 0; i < newHObjects.size(); i++) {
                loadedHitObjects.push_back(newHObjects[i]);
            }

            // Create new render object
            r->addRenderObject(RenderObject::createFromNote(currentTrack.notes[objToLoadIndex], currentSong));
            objToLoadIndex++;
        }

        // Unload hit objects
        while (loadedHitObjects.size() > 0 &&
        (int)loadedHitObjects[0]->getUnloadTime() + (int)loadingMargin <= getCurrentSongTime(now)) {
            Log::log("Unload");
            loadedHitObjects.erase(loadedHitObjects.begin());
            hitIndex--;
        }
        
        while (lEventToLoadIndex < currentTrack.events.size() &&
        currentSong->beatToOffset(currentTrack.events[lEventToLoadIndex].timing) <= getCurrentSongTime(now)) {
            auto event = currentTrack.events[lEventToLoadIndex];
            // Process local event

            lEventToLoadIndex++;
        }

        while (gEventToLoadIndex < currentSong->events.size() &&
        currentSong->beatToOffset(currentSong->events[gEventToLoadIndex].timing) <= getCurrentSongTime(now)) {
            auto event = currentSong->events[gEventToLoadIndex];
            // Process global event

            gEventToLoadIndex++;
        }

        // Check for misses
        for (int i = hitIndex; i < loadedHitObjects.size(); i++) {
            if (!loadedHitObjects[i]->hit && !loadedHitObjects[i]->miss && (int)loadedHitObjects[i]->getTime() + HIT_WINDOW_BAD < getCurrentSongTime(now)) {
                loadedHitObjects[i]->miss = true;
                hitIndex = i;
                Log::log("     --- Miss ---");
            }
        }

        // Handle input
        ControllerEvent event;
        while (inputHandler.getController(controllerNumber)->pollEvent(&event))
        {
            // Time of the input relative to the song
            const int relativeTime = getCurrentSongTime(event.timestamp);

            // Log::log("Controller#1 Event | rt: ", relativeTime, " ", event.str(), "\n");
            
            #ifdef PLAY_SOUNDS
                if (event.type == ControllerEvent::EVENT_PRESS) {
                    const unsigned int pitch = event.key + inputHandler.getController(controllerNumber)->transpose + TRANSPOSE;

                    if (pitch >= 0 && pitch < 128) {
                        sounds[pitch].setVolume((float)event.vel / 1.27f);
                        #ifdef IGNORE_VELOCITY
                            sounds[pitch].setVolume(100.f);
                        #endif
                        sounds[pitch].play();
                    }
                }
            #endif

            if (event.type == ControllerEvent::EVENT_PRESS) {

                //Log::log("Current Song Time: ", relativeTime, "ms");
                // Log::log("All notes (", currentTrack.notes.size(), "):");
                // for (unsigned int i = 0; i < currentTrack.notes.size(); i++) {
                //     Log::log("#", i, ": ", currentTrack.notes[i].timing.units, ", ", (unsigned int)currentTrack.notes[i].key, ", ", currentSong->beatToOffset(currentTrack.notes[i].timing), "ms");
                // }
                // Log::log("hitIndex: ", hitIndex, " loadedHitObjects.size(): ", loadedHitObjects.size());

                // Check for hits
                for (int i = hitIndex; i < loadedHitObjects.size(); i++) {

                    // Log::log("Checking note #", i, " / ", loadedHitObjects.size() - 1);
                    bool hit = false;

                    // Check if the note has already been hit or missed
                    if (!loadedHitObjects[i]->hit && !loadedHitObjects[i]->miss) {

                        //Log::log(": hittable");
                        // Check if note's pitch matches event's key
                        if (loadedHitObjects[i]->getKey() == event.key) {
                            //Log::log(": matches");
                            if ((int)relativeTime >= (int)loadedHitObjects[i]->getTime() - HIT_WINDOW_BAD &&
                                (int)relativeTime <= (int)loadedHitObjects[i]->getTime() + HIT_WINDOW_BAD) {
                                // Hit is in the bad hit window

                                if ((int)relativeTime >= (int)loadedHitObjects[i]->getTime() - HIT_WINDOW_GOOD &&
                                    (int)relativeTime <= (int)loadedHitObjects[i]->getTime() + HIT_WINDOW_GOOD) {    
                                    // Hit is in the good hit window

                                    if ((int)relativeTime >= (int)loadedHitObjects[i]->getTime() - HIT_WINDOW_PERFECT &&
                                        (int)relativeTime <= (int)loadedHitObjects[i]->getTime() + HIT_WINDOW_PERFECT) {
                                        // Hit is in the perfect hit window
                                        // Perfect Hit
                                        Log::log("+", HIT_SCORE_PERFECT, " --- Perfect --- ", (int)relativeTime - (int)loadedHitObjects[i]->getTime(), "ms");
                                        hit = true;
                                        score += HIT_SCORE_PERFECT;
                                    } else {
                                        // Good Hit
                                    Log::log("+", HIT_SCORE_GOOD, " --- Good --- ", (int)relativeTime - (int)loadedHitObjects[i]->getTime(), "ms");
                                        hit = true;
                                        score += HIT_SCORE_GOOD;
                                    }
                                } else {
                                    // Bad Hit
                                    Log::log("+", HIT_SCORE_BAD, " --- Bad --- ", (int)relativeTime - (int)loadedHitObjects[i]->getTime(), "ms");
                                    hit = true;
                                    score += HIT_SCORE_BAD;
                                }
                            } else {
                                // No hit
                            }
                        }
                    }

                    if (hit) {
                        loadedHitObjects[i]->hit = true;
                        hitIndex = i;
                        Log::log("Hit #", hitIndex, " Score: ", score);
                        Log::log("");
                        break;
                    }
                }
            }

        }

        // Update previousSongTime
        previousSongTime = getCurrentSongTime(now);

        // Check if window is open
        if (!r->win->isOpen()) {
            Global::halt();
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    // Stop playing music
    songMusic.stop();

    Log::log("Stopping main loop...");

    inputHandler.~InputHandler();

    for (unsigned char i = 0; i < 128; i++) {
        sounds[i].stop();
    }
    return;
}

std::chrono::system_clock::time_point Game::getStartTime() {
    return Global::getStartTimestamp();
}

Game::Game() {
    currentSong = nullptr;
    while (keyboardState.size() <= 127) {
        keyboardState.push_back(false);
    }
    keyboardState_old = std::vector<bool>(keyboardState);
}

Game::~Game() {
    Log::log("Destroying game object");
}