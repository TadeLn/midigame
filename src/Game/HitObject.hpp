#pragma once


#include <vector>

#include "Song/Song.hpp"
#include "Song/Note.hpp"


class HitObject {

public:
    static std::vector<HitObject*> createFromNote(Note, Song*);
    unsigned int getTime();
    unsigned int getUnloadTime();
    unsigned char getKey();
    bool hit;
    bool miss;

    HitObject();

private:
    unsigned int time;
    unsigned char key;

};