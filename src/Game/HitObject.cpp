#include "Game/HitObject.hpp"

#include <vector>

#include "Song/Song.hpp"
#include "Song/Note.hpp"


std::vector<HitObject*> HitObject::createFromNote(Note note, Song* songPtr) {
    std::vector<HitObject*> result;
    if (note.type == Note::NOTE_SHORT) {
        HitObject* newHObj = new HitObject();
        newHObj->time = songPtr->beatToOffset(note.timing);
        newHObj->key = note.key;
        result.push_back(newHObj);
    }
    return result;
}

unsigned int HitObject::getTime() {
    return time;
}

unsigned int HitObject::getUnloadTime() {
    return time + 0;
}

unsigned char HitObject::getKey() {
    return key;
}

HitObject::HitObject() {
    this->hit = false;
    this->miss = false;
}